import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:money_manager/models/transaction.model.dart';
import 'package:money_manager/widgets/chart.dart';
import 'package:money_manager/widgets/transaction-create.dart';
import 'package:money_manager/widgets/transaction-missing.dart';
import 'package:money_manager/widgets/transactions.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Money manager',
      theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch().copyWith(
              primary: Colors.deepOrange,
              secondary: const Color.fromRGBO(255, 217, 218, 1),
              onSecondary: Colors.black26),
          fontFamily: 'Quicksand',
          textTheme:
              const TextTheme(headline1: TextStyle(fontFamily: 'OpenSans'))),
      home: const MyHomePage(title: 'Money manager'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Transaction> transactions = [
    Transaction(
      id: DateTime.now(),
      title: "title",
      amount: 22.3,
      date: DateTime.now(),
    ),
    Transaction(
      id: DateTime.now(),
      title: "title",
      amount: 22.3,
      date: DateTime(2023, 1, 1),
    ),
    Transaction(
      id: DateTime.now(),
      title: "title",
      amount: 22.3,
      date: DateTime(2023, 1, 2),
    ),
  ];

  get recentTransactions {
    return transactions
        .where(
          (tx) =>
              tx.date.isAfter(DateTime.now().subtract(const Duration(days: 7))),
        )
        .toList();
  }

  _addTransaction(Transaction tx) {
    setState(() {
      transactions.add(tx);
    });

    Navigator.of(context).pop();
  }

  void _showTransactionCreateDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return GestureDetector(
            onTap: () {},
            behavior: HitTestBehavior.opaque,
            child: TransactionCreate(
              addTransaction: _addTransaction,
            ),
          );
        });
  }

  void _deleteTransaction(Transaction tx) {
    setState(() {
      transactions.removeWhere((transaction) {
        return transaction.id == tx.id;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.add))],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            transactions.isEmpty
                ? const TransactionMissing()
                : Column(
                    children: [
                      Chart(
                        recentTransactions: recentTransactions,
                      ),
                      Transactions(
                        transactions: transactions,
                        deleteTransaction: _deleteTransaction,
                      )
                    ],
                  )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () => _showTransactionCreateDialog(context),
          child: const Icon(Icons.add)),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
