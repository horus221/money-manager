import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:money_manager/models/transaction.model.dart';
import 'package:money_manager/widgets/transactions-item.dart';

class TransactionsList extends StatelessWidget {
  final List<Transaction> transactions;

  final Function deleteTransaction;

  const TransactionsList(
      {super.key, required this.transactions, required this.deleteTransaction});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 500,
      child: ListView.builder(
        itemBuilder: (context, index) => TransactionItem(
          transaction: Transaction(
            id: transactions[index].id,
            title: transactions[index].title,
            amount: transactions[index].amount,
            date: transactions[index].date,
          ),
          deleteTransaction: deleteTransaction,
        ),
        itemCount: transactions.length,
      ),
    );
  }
}
