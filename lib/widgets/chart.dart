import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:money_manager/models/transaction.model.dart';
import 'package:money_manager/widgets/chartbar.dart';

class Chart extends StatelessWidget {
  final List<Transaction> recentTransactions;

  List<Map<String, Object>> get groupedTransaction {
    return List.generate(7, (index) {
      var weekday = DateTime.now().subtract(Duration(days: index));
      double totalSum = 0;

      for (int i = 0; i < recentTransactions.length; i++) {
        if (recentTransactions[i].date.day == weekday.day &&
            recentTransactions[i].date.month == weekday.month &&
            recentTransactions[i].date.year == weekday.year) {
          totalSum += recentTransactions[i].amount;
        }
      }

      return {'day': DateFormat.E().format(weekday), 'amount': totalSum};
    });
  }

  double get totalTransaction {
    return recentTransactions.fold(
        0, (previousValue, element) => previousValue += element.amount);
  }

  const Chart({super.key, required this.recentTransactions});

  @override
  Widget build(BuildContext context) {
    print(groupedTransaction);
    return Card(
      margin: const EdgeInsets.all(10),
      elevation: 5,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ...groupedTransaction.map((tx) {
            return ChartBar(
                groupedTransaction: tx,
                heightFactor: (tx['amount'] as double) / totalTransaction);
          }).toList()
        ],
      ),
    );
  }
}
