import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class TransactionMissing extends StatelessWidget {
  const TransactionMissing({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const SizedBox(
          height: 20,
        ),
        Container(
          height: 400,
          child: Image.asset('assets/image/waiting.png'),
        ),
        const SizedBox(
          height: 20,
        ),
        const Text(
          "No transactions recorded",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
          ),
        )
      ],
    );
  }
}
