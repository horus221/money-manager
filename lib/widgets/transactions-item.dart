import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:money_manager/models/transaction.model.dart';

class TransactionItem extends StatelessWidget {
  final Transaction transaction;
  final Function deleteTransaction;
  const TransactionItem(
      {super.key, required this.transaction, required this.deleteTransaction});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        child: Padding(
          padding: const EdgeInsets.all(4),
          child: FittedBox(
              child: Text(
            '\$${transaction.amount}',
            style: const TextStyle(fontWeight: FontWeight.bold),
          )),
        ),
      ),
      title: Text(transaction.title),
      subtitle: Text(DateFormat.yMMMd().format(transaction.date)),
      trailing: IconButton(
        icon: const Icon(Icons.delete),
        onPressed: () => deleteTransaction(transaction),
      ),
    );
  }
}
