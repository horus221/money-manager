import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:money_manager/models/transaction.model.dart';
import 'package:money_manager/widgets/transactions-list.dart';

class Transactions extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTransaction;
  const Transactions(
      {super.key, required this.transactions, required this.deleteTransaction});

  @override
  Widget build(BuildContext context) {
    return TransactionsList(
      transactions: transactions,
      deleteTransaction: deleteTransaction,
    );
  }
}
