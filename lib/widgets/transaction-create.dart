import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:money_manager/models/transaction.model.dart';

class TransactionCreate extends StatefulWidget {
  final Function addTransaction;

  const TransactionCreate({super.key, required this.addTransaction});

  @override
  State<TransactionCreate> createState() => _TransactionCreateState();
}

class _TransactionCreateState extends State<TransactionCreate> {
  final titleController = TextEditingController();

  final amountController = TextEditingController();

  DateTime? selectedDate;

  void _openDatePicker() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2022),
            lastDate: DateTime.now())
        .then((value) {
      if (value == null) return;
      setState(() {
        selectedDate = value;
      });
    });
  }

  void onSubmit() {
    String title = titleController.text;
    double amount = double.parse(amountController.text);

    if (title.isEmpty || amount <= 0 || selectedDate == null) {
      return;
    }

    _addTransaction(title: title, amount: amount, date: selectedDate!);
  }

  void _addTransaction({
    required String title,
    required double amount,
    required DateTime date,
  }) {
    widget.addTransaction(
      Transaction(title: title, amount: amount, date: date, id: DateTime.now()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          TextField(
            controller: titleController,
            decoration: const InputDecoration(label: Text("Title")),
            onSubmitted: (_) => onSubmit(),
          ),
          TextField(
            controller: amountController,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(label: Text("Amount")),
            onSubmitted: (_) => onSubmit(),
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                  child: Text(selectedDate == null
                      ? "No date selected"
                      : DateFormat.yMMMMd().format(selectedDate!))),
              TextButton(
                onPressed: _openDatePicker,
                child: Text(
                  "Add a date",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.primary),
                ),
              )
            ],
          ),
          const SizedBox(height: 10),
          ElevatedButton(
              onPressed: onSubmit, child: const Text("Add transaction"))
        ],
      ),
    );
  }
}
